<!DOCTYPE html>
<html>
<head>
    <title>Matrix 3D</title>
    <link rel="stylesheet" media="screen" href="assets/css/main.css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
</head>
<body>
    <div class="container">

        <header>
           <h1><a href="https://www.hackerrank.com/challenges/cube-summation/problem" target="_BLANK">Matrix 3D</a></h1>
        </header>
        
        <form id="matrix3d" action="" method="POST">
            <nav>
                Número de casos de Prueba <span class="required">*</span> <br><input type="number" id="testCases"  name="testCases" size="10" min="1" max="50" step="1" pattern="\d+" placeholder="1-50"><br>
                <hr>
                <button class="btn" id="start-matrix" type="button">Iniciar Matriz</button>
                <button class="btn" style="background: #CCC !important; display:none" id="restart-process" type="button">Reiniciar Proceso</button>
                <hr>
                <h3 class="required">Restricciones en las operaciones</h3>
                <strong>T:</strong>Casos de Prueba<br>
                <strong>N:</strong>Número de Columnas<br>
                <strong>M:</strong>Número de Operaciones<br>
                
                <ul>
                  <li>1 <= T <= 50</li>
                  <li>1 <= N <= 100</li>
                  <li>1 <= M <= 1000</li>
                  <li>1 <= x1 <= x2 <= N</li>
                  <li>1 <= y1 <= y2 <= N</li>
                  <li>1 <= z1 <= z2 <= N</li>
                  <li>1 <= x,y,z <= N</li>
                  <li>-10<sup>9</sup> <= W <= 10<sup>9</sup></li>
                </ul>
            </nav>
        

        <article>
          <div class="matrix">
              <h1>Acá se Mostrará el resultado de las operaciones realizadas en la aplicación.</h1>
          </div>
          <div class="result-finale">
          </div>
        </article>
        </form>

        <footer>Julián Raigosa R. - jerr919@gmail.com</footer>

    </div>
</body>
</html>