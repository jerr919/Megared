<?php

class ViewController 
{
    private $render = FALSE;

    public function __construct($template)
    {
        try {
            $file =  $_SERVER['DOCUMENT_ROOT'] . '/views/' . strtolower($template) . '.php';

            if (file_exists($file)) {
                $this->render = $file;
            } else {
                throw new \Exception('Template ' . $template . ' not found!');
            }
        } catch (\Exception $e) {
            echo $e->errorMessage();
        }
    }

    public function assign($variable, $value)
    {
        $this->data[$variable] = $value;
    }

    public function __destruct()
    {
        include($this->render);
    }
}
