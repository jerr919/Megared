<?php
include 'ViewController.php';
include $_SERVER['DOCUMENT_ROOT'] . '/models/Matrix.php';

class MainController
{
    /**
     * Renders form so the user can input his data.
     * @return void
     */
    public function index() 
    {
        $view = new ViewController('form');
    }

    /**
     * Initialize the matrix, processes and give back the result to the user.
     * @return void
     */
    public function createMatrix()
    {
        $t        = isset($_REQUEST['testCases']) ? $_REQUEST['testCases'] : NULL;
        $form     = isset($_REQUEST['TestCase']) ? $_REQUEST['TestCase'] : NULL;
        $response = ['status' => '200', 'message' => '', 'matrix' => ''];

        if (isset($form) && count($form)) {
            try {
                $result  = '<h1>Resultado</h1>';
                $tcIndex = 1;

                foreach ($form as $testCase) {
                    $result .= "<h2>Caso de Prueba $tcIndex</h2>";
                    $n = $testCase['n'];
                    $m = $testCase['m'];
                    unset($testCase['n'], $testCase['m']);
                    
                    $Matrix = new Matrix($n, $m); 

                    $opQindex = 1;
                    foreach ($testCase as $index => $operation) {
                        $opResult = $Matrix->processOperation($operation, $tcIndex, $index);
                        if (count($operation) == 6) {
                            $result .= "<h3>Query $opQindex: " . $opResult . '</h3>';
                            $opQindex++;
                        }
                    }
                    $tcIndex++;
                }

                $response['matrix'] = $result;
            } catch (\Exception $e) {
                $response['status']  = 'error';
                $response['message'] = $e->getMessage();
            }
        } else {
            $response['status']  = 'error';
            $response['message'] = 'No hay información para procesar.';
        }
        
        echo json_encode($response);
    }
}
