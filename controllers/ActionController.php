<?php
include 'MainController.php';

$type           = isset($_REQUEST['type']) ? $_REQUEST['type'] : NULL;
$MainController = new MainController();

if (isset($type)) {
    switch ($type) {
        case 'createMatrix':
            $MainController->createMatrix();
            break;
    }
} else {
    throw new \Exception("Solicitud Invalida.");
}