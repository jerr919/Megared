<?php 

class Matrix
{
    /**
     * Density of the Matrix
     * @var integer
     */
    public $n;   

    /**
     * Amount of operations allowed
     * @var integer
     */
    public $m;

    /**
     * Matrix
     * @var array
     */
    public $matrix3D = [];

    public function __construct($n, $m)
    {
        if ($n < 1 || $n > 100) {
            throw new \Exception("El tamaño de la matriz es invalida, debe ingresar" . 
            " un número entre 1 y 100");
        }

        if ($m < 1 || $m > 1000) {
            throw new \Exception("El tamaño de la matriz es invalida, debe ingresar" . 
            " un número entre 1 y 100");
        }

        $this->n = $n;
        $this->m = $m;
        
        for($x = 1; $x <= $this->n; $x++) {
            for($y = 1; $y <= $this->n; $y++) {
                for($z = 1; $z <= $this->n; $z++) {
                    $this->matrix3D[$x][$y][$z] = 0;
                }
            }
        }
    }

    /**
     * Processes the operation given
     * @param  Array $operation
     * @param  integer $testCaseNumber
     * @param  integer $operationIndex
     * @return string | integer, if the operation is an update the result will be
     * an empty string, if the operation is a query then the result will be an integer.
     */
    public function processOperation($operation, $testCaseNumber, $operationIndex)
    {
        $result = '';
        
        if (count($operation) == 4) {
            $this->updateValue($operation, $testCaseNumber, $operationIndex);
        } else {
            $result = $this->getQuery($operation, $testCaseNumber, $operationIndex);
        }

        return $result;
    }

    /**
     * Updates a cell value of the matrix
     * @param  Array $operationData
     * @param  integer $testCaseNumber
     * @param  integer $operationIndex
     * @return boolean.
     */
    public function updateValue($operationData, $testCaseNumber, $operationIndex) 
    {
        $x = $operationData['x'];
        $y = $operationData['y'];
        $z = $operationData['z'];
        $w = $operationData['w'];
        $limitInfW = pow(-10, 9);
        $limitSupW = pow(10, 9);
        unset($operationData['w']);
        
        foreach ($operationData as $key => $value) {
            if ($value < 1 || $value > $this->n) {
                throw new \Exception("El valor de $key en el caso de prueba " . 
                    "$testCaseNumber operación $operationIndex es Invalido, debe ser un " . 
                    "valor entre 1 y {$this->n}");
            }
        }

        if ($w < $limitInfW || $w > $limitSupW) {
            throw new \Exception("El valor de la celda a actualizar en el caso de prueba " . 
                    "$testCaseNumber operación $operationIndex es Invalido, debe ser un " . 
                    "valor entre $limitInfW y $limitSupW");
        }

        $this->matrix3D[$x][$y][$z] = $w;
        return TRUE;
    }

    /**
     * Obtains the addition between a cell range given.
     * @param  Array $queryRange
     * @param  integer $testCaseNumber
     * @param  integer $operationIndex
     * @return integer
     */
    public function getQuery($queryRange, $testCaseNumber, $operationIndex)
    {
        $value = 0;
        $x1 = $queryRange['x1'];
        $x2 = $queryRange['x2'];
        $y1 = $queryRange['y1'];
        $y2 = $queryRange['y2'];
        $z1 = $queryRange['z1'];
        $z2 = $queryRange['z2'];
        $n = $this->n;

        if ($x1 >= 1 && $x1 <= $x2 && $y1 >= 1 && $y1 <= $y2 && $z1 >= 1 && $z1 <= $z2 &&
                $x2 <= $n && $y2 <= $n && $z2 <= $n) {
            for ($x = $x1; $x <= $x2; $x++)
                for ($y = $y1; $y <= $y2; $y++)
                    for ($z = $z1; $z <= $z2; $z++)
                        $value += $this->matrix3D[$x][$y][$z];            
        } else {
            throw new \Exception("Los parametros dados para la operación $operationIndex" . 
                    " en el caso de prueba $testCaseNumber son invalidos por favor revise las restricciones dadas " . 
                    "y los valores ingresados.");
        }

        return $value;
    }
}