$(function() {
    $(document.body).on('click', '#restart-process', function() {
        if (confirm('¿Está seguro(a) que desea reiniciar el proceso?')) {
            location.reload(true);
        }
    });

    $(document.body).on('click', '#ProcessButton', function() {
        processMatrix();
    });

    $(document.body).on('click', '#start-matrix', function() {
        let t = Number($('#testCases').val());

        if (!(t) || t < 1 || t > 50) {
            alert('Debe ingresar un valor para los casos de prueba y debe ' + 
                'ser un número entero entre 1 y 50.');
            return false;
        }
        $('.matrix').html('');
        $('#testCases').attr('readOnly', true);
        $('#start-matrix').fadeOut(400, function() {
            $('#restart-process').fadeIn(400);    
        });
        
        createParams(t);
    }); 

    $(document.body).on('click', '.op-loader', function() {
        let Button = $(this);
        let ParentDiv = $('div#' + Button.attr('data-rel'));
        let valid = true;
        
        ParentDiv.find('input.required').each(function(i, item) {
            if (!item.value) {
                alert('Debe ingresar un valor para el tamaño de la matriz y cantidad de operaciones!');
                valid = false;
                return false;
            }
        });

        if (valid) {
            let n = Number(ParentDiv.find('input.n').val());
            let m = Number(ParentDiv.find('input.m').val());

            if (n < 1 || n > 100 || m < 1 || m > 100) {
                alert('Número de columnas debe estar entre 1 y 100 y Número de Ooperaciones debe estar entre 1 y 1000');
                return false;
            }

            ParentDiv.find('input, button').addClass('readOnly');
            createOperations(m, ParentDiv);
        }
    });

    $(document.body).on('click', '.operation', function() {
        let Item = $(this);
        let InputContainer = Item.parent().parent('div').find('div.' + Item.attr('data-rel'));

        switch(Item.val()) {
            case 'update':
                createUpdateInputs(Item, InputContainer);
                break;

            case 'query':
                createQueryInputs(Item, InputContainer);
                break;

            default:
                InputContainer.html('');
                break;
        }
    });
});

/**
 * Creates the inputs of the query operation.
 * @param  {DOMElement} SelectItem     
 * @param  {DOMElement} InputContainer 
 * @return void
 */
function createQueryInputs(SelectItem, InputContainer) {
    InputContainer.html('');
    let testCase = SelectItem.attr('data-testcase');
    let operation = SelectItem.attr('data-operation');
    
    let ContainerX1 = document.createElement('div');
    ContainerX1.style = 'width:16%;float:left';
    let InputX1 = document.createElement('input');
    InputX1.type = 'number';
    InputX1.name = 'TestCase[' + testCase +'][' + operation + '][x1]';
    InputX1.placeholder = 'X1';
    ContainerX1.appendChild(InputX1);

    let ContainerY1 = document.createElement('div');
    ContainerY1.style = 'width:16%;float:left';
    let InputY1 = document.createElement('input');
    InputY1.type = 'number';
    InputY1.name = 'TestCase[' + testCase +'][' + operation + '][y1]';
    InputY1.placeholder = 'Y1';
    ContainerY1.appendChild(InputY1);

    let ContainerZ1 = document.createElement('div');
    ContainerZ1.style = 'width:16%;float:left';
    let InputZ1 = document.createElement('input');
    InputZ1.type = 'number';
    InputZ1.name = 'TestCase[' + testCase +'][' + operation + '][z1]';
    InputZ1.placeholder = 'Z1';
    ContainerZ1.appendChild(InputZ1);

    let ContainerX2 = document.createElement('div');
    ContainerX2.style = 'width:16%;float:left';
    let InputX2 = document.createElement('input');
    InputX2.type = 'number';
    InputX2.name = 'TestCase[' + testCase +'][' + operation + '][x2]';
    InputX2.placeholder = 'X2';
    ContainerX2.appendChild(InputX2);

    let ContainerY2 = document.createElement('div');
    ContainerY2.style = 'width:16%;float:left';
    let InputY2 = document.createElement('input');
    InputY2.type = 'number';
    InputY2.name = 'TestCase[' + testCase +'][' + operation + '][y2]';
    InputY2.placeholder = 'Y2';
    ContainerY2.appendChild(InputY2);

    let ContainerZ2 = document.createElement('div');
    ContainerZ2.style = 'width:16%;float:left';
    let InputZ2 = document.createElement('input');
    InputZ2.type = 'number';
    InputZ2.name = 'TestCase[' + testCase +'][' + operation + '][z2]';
    InputZ2.placeholder = 'Z2';
    ContainerZ2.appendChild(InputZ2);

    InputContainer[0].appendChild(ContainerX1);
    InputContainer[0].appendChild(ContainerY1);
    InputContainer[0].appendChild(ContainerZ1);
    InputContainer[0].appendChild(ContainerX2);
    InputContainer[0].appendChild(ContainerY2);
    InputContainer[0].appendChild(ContainerZ2);
}


/**
 * Creates the inputs of the update operation.
 * @param  {DOMElement} SelectItem     
 * @param  {DOMElement} InputContainer 
 * @return void
 */
function createUpdateInputs(SelectItem, InputContainer) {
    InputContainer.html('');
    let testCase = SelectItem.attr('data-testcase');
    let operation = SelectItem.attr('data-operation');
    let ContainerX = document.createElement('div');
    ContainerX.style = 'width:25%;float:left';

    let InputX = document.createElement('input');
    InputX.type = 'number';
    InputX.name = 'TestCase[' + testCase +'][' + operation + '][x]';
    InputX.placeholder = 'X';

    ContainerX.appendChild(InputX);

    let ContainerY = document.createElement('div');
    ContainerY.style = 'width:25%;float:left';
    let InputY = document.createElement('input');
    InputY.type = 'number';
    InputY.name = 'TestCase[' + testCase +'][' + operation + '][y]';
    InputY.placeholder = 'Y';

    ContainerY.appendChild(InputY);

    let ContainerZ = document.createElement('div');
    ContainerZ.style = 'width:25%;float:left';
    let InputZ = document.createElement('input');
    InputZ.type = 'number';
    InputZ.name = 'TestCase[' + testCase +'][' + operation + '][z]';
    InputZ.placeholder = 'Z';

    ContainerZ.appendChild(InputZ);

    let ContainerW = document.createElement('div');
    ContainerW.style = 'width:25%;float:left';
    let InputW = document.createElement('input');
    InputW.type = 'number';
    InputW.name = 'TestCase[' + testCase +'][' + operation + '][w]';
    InputW.placeholder = 'W';

    ContainerW.appendChild(InputW);
    

    InputContainer[0].appendChild(ContainerX);
    InputContainer[0].appendChild(ContainerY);
    InputContainer[0].appendChild(ContainerZ);
    InputContainer[0].appendChild(ContainerW);
}

/**
 * Creates the operations according to the number given.
 * @param {Number} m.
 * @param {DOMElement} ParentDiv
 * @return void
 */
function createOperations(m, ParentDiv) {
    let testCase = ParentDiv.attr('id').split('container-')[1];
    let OperationDiv = ParentDiv.find('div.operations')[0];
    OperationDiv.style = 'display:block';

    for (let i = 1; i <= m; i++) {
        let Row =  document.createElement('div');
        Row.style = 'width:100%;float:left';

        let OptionEmpty   = document.createElement('option');
        OptionEmpty.text  = 'Seleccione..'; 
        OptionEmpty.value = '';
        let OptionUpdate   = document.createElement('option');
        OptionUpdate.text  = 'Update'; 
        OptionUpdate.value = 'update'; 
        let OptionQuery    = document.createElement('option');
        OptionQuery.text   = 'Query'; 
        OptionQuery.value  = 'query'; 

        let OpTypeInput = document.createElement('select');
        OpTypeInput.className = 'operation';
        OpTypeInput.dataset.testcase = testCase;
        OpTypeInput.dataset.operation = i;
        OpTypeInput.dataset.rel = 'input-operation-container-' + i;
        OpTypeInput.size = '30%';

        OpTypeInput.add(OptionEmpty);
        OpTypeInput.add(OptionUpdate);
        OpTypeInput.add(OptionQuery);

        let OpTypeContainer = document.createElement('div');
        OpTypeContainer.style = 'width:30%;float:left';
        OpTypeContainer.appendChild(OpTypeInput);

        let DivInputContainer = document.createElement('div');
        DivInputContainer.className = 'input-operation-container-' + i;
        DivInputContainer.style = 'width:70%;float:left';
        Row.appendChild(OpTypeContainer);
        Row.appendChild(DivInputContainer);
        OperationDiv.appendChild(Row);
    }
}

/**
 * Creates the form fields for the matrix creation and its operations.
 * @param  {Number} testCases
 * @return void
 */
function createParams(testCases) {
    let Matrix = document.querySelector('.matrix');
    let ProcessButon = document.createElement('button');
    ProcessButon.type = 'button';
    ProcessButon.id = 'ProcessButton';
    ProcessButon.className = 'btn';
    ProcessButon.innerHTML = 'Procesar Matrices';
    
    for (let i = 1; i <= testCases; i++) {

        let Container = document.createElement('div');
        Container.className = 'tc-container';
        Container.id = 'container-' + i;

        let Title = document.createElement('h1');
        Title.innerHTML = 'Caso de Prueba ' + i;

        let LabelN = document.createElement('label');
        LabelN.innerHTML = 'Número de Columnas:';

        let InputN = document.createElement('Input');
        InputN.name = 'TestCase[' + i + '][n]';
        InputN.type = 'number';
        InputN.className = 'required n';
        InputN.placeholder = '1-100';

        let LabelM = document.createElement('label');
        LabelM.innerHTML = 'Número de Operaciones:';
        let InputM = document.createElement('Input');
        InputM.name = 'TestCase[' + i + '][m]';
        InputM.type = 'number';
        InputM.className = 'required m';
        InputM.placeholder = '1-1000';
        
        let OpButton = document.createElement('button');
        OpButton.type = 'button';
        OpButton.innerHTML = 'Cargar Operaciones';
        OpButton.dataset.rel = 'container-' + i;
        OpButton.className = 'btn-primary op-loader';

        let OpContainer = document.createElement('div');
        OpContainer.className = 'operations';
        OpContainer.style = 'width:100%;display:none';

        Container.appendChild(Title);
        Container.appendChild(LabelN);
        Container.appendChild(InputN);
        Container.appendChild(LabelM);
        Container.appendChild(InputM);
        Container.appendChild(OpButton);
        Container.appendChild(OpContainer);

        Matrix.appendChild(Container);
    }
    Matrix.appendChild(ProcessButon);

}

/**
 * Processes the input entered by user in order to obtain the result
 * @return void
 */
function processMatrix() {
     $.ajax({
            url: 'controllers/ActionController.php?type=createMatrix',
            type: 'POST',
            data: $('#matrix3d').serialize(),
            dataType: 'JSON',
            beforeSend: function(jqXHR) {
                $('#ProcessButton').html('Procesando...')
                    .css('pointer-events', 'none')
                    .attr('disabled', true);
            },
            success: function(response) {
                if (response.status == '200') {
                    $('.result-finale').html(response.matrix);
                    $('#ProcessButton').html('Iniciar Matriz')
                        .css('pointer-events', 'auto')
                        .attr('disabled', false);
                } else {
                    $('#ProcessButton').html('Procesar Matrices')
                        .css('pointer-events', 'auto')
                        .attr('disabled', false);
                    alert(response.message);
                }
            },
            error: function(jqXHR) {
                $('#ProcessButton').html('Procesar Matrices')
                    .css('pointer-events', 'auto')
                    .attr('disabled', false);
                alert('Ha ocurrido un error con su solicitud, ' + 
                    'intentelo más tarde o consulte el área de soporte');
            }
        });
}